# adsms

Send SMS aircraft alerts based on ADS-B data

![Screenshot of text sent by adsms](screenshot.jpg)

## Usage

Copy the configuration file, make any necessary changes, and run:

    ./adsms.py <configuration_file>

## Configuration file

All values are required. All are strings unless otherwise specified. All times
are in seconds.

* `textbelt_key`: your [Textbelt](https://textbelt.com) API key. This is always
  required, but can be set to an invalid value if you do not plan to use SMS.
* `data`: a URL to a readsb/tar1090 `aircraft.json` endpoint (for a local
  tracker), or an [adsb.one](https://adsb.one)-style `/hex/` API endpoint. For
  adsb.one, this should be `https://api.adsb.one/v2/hex/`. The final slash is
  important.
* `codes` (boolean): whether or not to append ICAO codes to the URL (`false`
  for local tracker; `true` for adsb.one API)
* `tracker`: a URL to a tar1090 tracker (e.g. https://globe.theairtraffic.com/)
* `database`: an SQLite file in which to store subscriptions
* `pid_file`: path to which to write the PID (set to empty string to not write
  a PID file)
* `max_age`: maximum age of aircraft pings; pings older than this will be
  ignored
* `min_disappearance`: the minimum time for which an aircraft must go "off the
  radar" before disappearing for new pings to trigger notifications again
* `delay`: time to wait after processing all rules before running the loop
  again

## Database Schema

`adsms` uses a SQLite database to store subscriptions and information about
tracked aircraft. Currently, the only table is `subscriptions`.

### `subscriptions`

The `subscriptions` table has the following columns:

| Column Name   | Data Type | Description                                                      |
| ------------- | --------- | ---------------------------------------------------------------- |
| `rowid`       | INTEGER   | Unique identifier for the subscription.                          |
| `phone`       | TEXT      | Identifier to receive notifications for this subscription.       |
| `icao`        | TEXT      | ICAO address of the aircraft to track.                           |
| `description` | TEXT      | Description of the aircraft being tracked.                       |
| `last_seen`   | INTEGER   | Timestamp of the last time this aircraft was seen by the system. |
| `platform`    | TEXT      | The method by which to send the message.                         |
| `min_lat`     | REAL      | The minimum latitude of the geofence.                            |
| `min_lon`     | REAL      | The minimum longitude of the geofence.                           |
| `max_lat`     | REAL      | The maximum latitude of the geofence.                            |
| `max_lon`     | REAL      | The maximum longitude of the geofence.                           |

This table stores information about each subscription, including the contact
information to send notifications to, the ICAO address of the aircraft to
track, a description of the aircraft, and the last time it was seen by the
system.

adsms can send messages by SMS using [Textbelt](https://textbelt.com) or by
Discord using webhooks. For SMS, use `textbelt` for `platform` and the phone
number for `phone`; for Discord, use `discord_webhook` for `platform` and the
webhook URL for `phone`. (The field is called `phone` because adsms originally
only supported SMS.)

When adding new entries, set `last_seen` to 0.

If you want to notify whenever an aircraft is seen anywhere, use -90 for
`min_lat`, -180 for `min_lon`, 90 for `max_lat`, and 180 for `max_lon`. This
will cover the entire globe.

## Use of ChatGPT 

Portions of both this README and the `adsms` code have been partially written with ChatGPT.
