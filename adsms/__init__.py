# Partially written with ChatGPT

import os
import time
import json
import argparse
import requests
import adsms.db
import discord_webhook


def load_config(config_file):
    with open(config_file) as f:
        return json.load(f)


def send_text_message(phone, message, key):
    request = {"phone": phone, "message": message, "key": key}
    resp = requests.post("https://textbelt.com/text", request)
    print(resp.json())


def process_subscriptions(con, config, data):
    subscriptions = db.get_subscriptions(con)

    for (
        sub_id,
        phone,
        icao,
        description,
        last_seen,
        platform,
        min_lat,
        min_lon,
        max_lat,
        max_lon,
    ) in subscriptions:
        if (
            icao in data
            and data[icao]["seen_pos"] < config["max_age"]
            and min_lat <= data[icao]["lat"] <= max_lat
            and min_lon <= data[icao]["lon"] <= max_lon
        ):
            if last_seen + config["min_disappearance"] < time.time():
                message = f"{description}\n{config['tracker']}?icao={icao}"

                if platform == "textbelt":
                    send_text_message(phone, message, config["textbelt_key"])
                elif platform == "discord_webhook":
                    print(
                        discord_webhook.DiscordWebhook(
                            url=phone, content=message
                        ).execute()
                    )

                print(f"{phone}: {message}")

            db.update_last_seen_time(con, sub_id)

    con.commit()


def get_current_data(con, config):
    # return {"78007e": {"seen": 0}}
    print(
        config["data"]
        + (",".join(db.get_all_icao(con)) if config["codes"] else "")
    )
    response = requests.get(
        config["data"]
        + (",".join(db.get_all_icao(con)) if config["codes"] else "")
    ).json()
    planes = response.get("aircraft", response["ac"])
    return {plane["hex"]: plane for plane in planes}


def run(config):
    con = adsms.db.load_database(config["database"])

    print(db.get_all_icao(con))

    while True:
        data = get_current_data(con, config)

        process_subscriptions(con, config, data)

        time.sleep(config["delay"])

    con.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("config_file")
    args = parser.parse_args()

    config = load_config(args.config_file)

    try:
        if config["pid_file"]:
            with open(config["pid_file"], "w+") as f:
                f.write(str(os.getpid()))

        run(config)
    finally:
        if config["pid_file"]:
            os.unlink(config["pid_file"])
