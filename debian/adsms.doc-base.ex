Document: adsms
Title: Debian adsms Manual
Author: Samuel Sloniker <sam@kj7rrv.com>
Abstract: This manual describes what adsms is
 and how it can be used to send SMS notifications
 based on ADS-B data.
Section: unknown

Format: Text
Files: /usr/share/doc/adsms/README.md
